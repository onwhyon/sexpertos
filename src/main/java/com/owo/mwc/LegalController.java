package com.owo.mwc;

import javax.servlet.http.HttpServletRequest;

import com.owo.mwc.parsed.Article2Col;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.owo.dao.domain.Constants;
import com.owo.mwc.parsed.HeaderMenu;

@Controller
public class LegalController extends SexpertosController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	Article2Col article2col;

	@RequestMapping(value={"gestionarsuscripcion.htm*", "/gestionar/"})
	public String gestionarSubscription(Model model, HttpServletRequest request) {
		String sRet = "transfer:/svsession/";
		checkCommons(request);
		if (ows != null) {
			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);
			model.addAttribute("gestion", true);
			model.addAttribute("legaltitle", "GESTIONAR SUBSCRIPCI�N");
			ses.setAttribute(Constants.LASTPAG, "/gestionarsuscripcion.htm");
			sRet = ows.getGama() + "legal";
		}
		return sRet;
	}

	@RequestMapping(value={"condicionesservicio.htm*", "/condiciones/"})
	public String condicionesServicio(Model model, HttpServletRequest request) {
		String sRet = "forward:/index.htm";
		checkCommons(request);
		if (ows != null) {
			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);
			model.addAttribute("gestion", false);
			model.addAttribute("legaltitle", "CONDICIONES DEL SERVICIO");
			ses.setAttribute(Constants.LASTPAG, "/condicionesservicio.htm");

			article2col.paramCat();
			article2col.setCategory(cMenu, listMenu);
			article2col.getParsed(model, param);


			sRet = ows.getGama() + "legal";
		}
		return sRet;
	}
	
	@RequestMapping(value={"promo.htm*"})
	public String promocion(Model model, HttpServletRequest request) {
		String sRet = "forward:/index.htm";
		checkCommons(request);
		if (ows != null) {
			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);
			model.addAttribute("legaltitle", "Bases legales, promocion DECATHLON");
			ses.setAttribute(Constants.LASTPAG, "/promo.htm");
			sRet = ows.getGama() + "promo";
		}
		return sRet;
	}

}
