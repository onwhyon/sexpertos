package com.owo.mwc;

import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.owo.dao.domain.Poll;
import com.owo.dao.repository.content.ContentDao;

@Controller
public class PollController {

	@Autowired
	ContentDao cntdao;

	@RequestMapping(value="/poll.htm", method=RequestMethod.POST)
	public String procVote(Model model,
			@RequestParam(required = false, value = "ok") String ok,
			@RequestParam(required = false, value = "ko") String ko,
			@RequestParam(value = "idContent") long id) {
		Poll poll;
		try {
			poll = cntdao.getPoll(id);
		} catch (NoResultException nre) {
			poll = new Poll();
			poll.setIdContent(id);
		}
		if (StringUtils.isNotEmpty(ok)) {
			poll.setOks(poll.getOks()+1);
		}

		poll.setTotal(poll.getTotal()+1);
		Poll p = cntdao.savePoll(poll);
		double i = p.getOks()*100/p.getTotal();
		String sRet = (new StringBuilder("p").append(Math.round(i/10)*10)).toString();
		model.addAttribute("pollbar", sRet);

		return "redirect:/index.htm";
	}

	@RequestMapping(value="/poll.htm", method=RequestMethod.GET)
	public void nada() {}
}
