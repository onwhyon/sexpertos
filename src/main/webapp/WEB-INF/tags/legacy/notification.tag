<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="user" description="Nombre del usuario si está reconocido" %>
<%@ attribute name="text" description="Nombre del usuario si está reconocido" %>

<c:if test="${fn:length(user)>0 || fn:length(text)>0 }">
 <table>
  <tr>
   <td class="small-notification green-notification">
    <c:choose>
     <c:when test="${fn:length(user) > 0 }">
      <p><b>Bienvenid@ de nuevo ${user }</b></p>
     </c:when>
     <c:when test="${fn:length(text) > 0 }">
      <p><b>${text}</b></p>
     </c:when>
    </c:choose>
    <a href="#">x</a>   
   </td>
  </tr>
 </table>
</c:if>