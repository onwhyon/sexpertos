<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Muestra un banner con un texto debajo si se le pasa como atributo, el atributo rect=true indica si se quiere el banner con un borde, formato vertical: banner, [texto]" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="image" description="Banner que se va a mostrar" %>
<%@ attribute name="imgalt" description="Alt del banner" %>
<%@ attribute name="text" description="texto debajo del banner" %>
<%@ attribute name="title" description="texto debajo del banner" %>
<%@ attribute name="rect"
	description="Hay dos tipos de banner uno con recuadro y otro sin el este booleano determina cual se dibuja" %>
	
<%@ attribute name="link" %>
<%@ attribute name="friendurl" description="" %>
<%@ attribute name="pag" %>
<%@ attribute name="cat" %>

<c:if test="${fn:length(link)!=0 }"><c:set var="itemlink" value="premium/${link}" /></c:if>
<c:if test="${fn:length(friendurl)!=0 }"><c:set var="itemlink" value="${pag }/${friendurl}" /></c:if>

<c:if test="${image != null && fn:length(image) > 0 }">
	<div class="column moduloPrincipal">
		<c:if test="${fn:length(itemlink) > 0 }">
			<a href="${pageContext.request.contextPath}/${itemlink }">
		</c:if>
		<div>

		<img src="<%=IMGS_PATH %>${image }" alt="${imgalt }">
		<c:if test="${fn:length(itemlink) > 0 }">
			</a>
		</c:if>
		</div>
		<c:if test="${fn:length(cat) > 0 }">
			<h3>${cat }</h3>
		</c:if>
		<h4>${title }</h4>
		<c:if test="${fn:length(text) > 0 }">
			<p class="no-bottom grosso">
				${text } <a class="ver_mas margin-top"  href="${pageContext.request.contextPath}/${itemlink }">Leer [+]</a>
			</p>
		</c:if>
	</div>

</c:if>
