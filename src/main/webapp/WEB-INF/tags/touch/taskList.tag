<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Muestra un encabezado, seguido de una lista de elementos extraidos de una iterador" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="title" description="Texto del encabezado de la lista" %>
<%@ attribute name="list" description="Elementos que se mostrarán en el UL" %>

<c:if test="${fn:length(list) > 0 }">
	<div class="container  no-bottom">
		<c:if test="${fn:length(title) > 0 }">
			<h4 class="uppercase left-text">${title }</h4>
		</c:if>
		<c:forTokens items="${list }" delims="#" var="item">
			<c:choose>
				<c:when test="${!fn:contains(item, '|') }">
					<h4 class="uppercase left-text">${item }</h4>
				</c:when>
				<c:when test="${fn:contains(item, '|') }">
					<ul>
						<c:forTokens items="${item }" delims="|" var="itemlist">
							<li>${itemlist }</li>
						</c:forTokens>
					</ul>
				</c:when>
			</c:choose>
		</c:forTokens>
	</div>
</c:if>