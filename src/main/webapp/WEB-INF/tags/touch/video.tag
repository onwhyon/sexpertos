<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Muestra un banner con un texto debajo si se le pasa como atributo, el atributo rect=true indica si se quiere el banner con un borde, formato vertical: banner, [texto]" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="video" %>
<%@ attribute name="title"  %>

	<video width="100%" controls preload poster="${pageContext.request.contextPath}/resources/images/misc/logo_play.jpg">

		<source src="<%=VIDS_PATH %>${video }.webm" type="video/webm"> </source>
		<source src="<%=VIDS_PATH %>${video }.mp4" type="video/mp4"> </source>
		<source src="<%=VIDS_PATH %>${video }.ogv" type="video/ogg"> </source>
		<!--Navegadores sin HTML5 distintos de IE-->
		<object data="<%=VIDS_PATH %>${video }.mp4" width="100%"> Sin video
			<param name="autoplay" value="true">
		</object>
	</video>

	<div class="video">
		<h3>${title }</h3>
	</div>



