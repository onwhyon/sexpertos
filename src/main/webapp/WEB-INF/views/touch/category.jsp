<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/touch" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headTouch.jsp" %>
	<body class="categoriaBody">

		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }" 
		list="${headermenucats }" menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />


		<c:if test="${videos==true}">
			<div class="moduloA videoTop">
				<owo:listVideos list="${article2colos_1_3 }" pag="<%=PRM + CNT %>"/>
			</div>
		</c:if>
		<c:if test="${videos==false}">
			<div class="moduloA">
				<owo:banner image="${banner_1_101_1 }"
				imgalt="${banner_1_016_1 }" title="${banner_1_002_1 }"
				link="${banner_1_015_1 }" friendurl="${banner_1_020_1 }"
				pag="<%=PRM + CNT %>" cat="${banner_1_017_1 }" text="${banner_1_005_1 }"/>

				<owo:listVideos list="${article2colos_1_4 }" pag="<%=PRM + CNT %>"/>

				<owo:article1rowBcnts list="${article2colos_1_1 }" pag="<%=PRM + CNT %>" />
			</div>

		</c:if>
		<div class="artNavi">
			<c:if test="${prev==true }">
				<a href="${prvpage}"><span>Anterior</span></a>
			</c:if>
			<c:if test="${prev==false }">
				<a href="${prvpage}" style="display: none"><span>Anterior</span></a>
			</c:if>
			<c:if test="${sig==true }">
				<a href="${nxtpage}"><span>Siguiente</span></a>
			</c:if>
			<c:if test="${sig==false }">
				<a href="${nxtpage}" style="display: none"><span>Siguiente</span></a>
			</c:if>
		</div>


		<owo:catList1col list="${article2col_0 }" pag="<%=CAT %>"/>
		</div>
	</body>
</html>