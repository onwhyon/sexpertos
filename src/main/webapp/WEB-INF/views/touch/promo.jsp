﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/touch" prefix="owo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="../inc/ctes.jsp"%>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ include file="../inc/headTouch.jsp"%>
<body>

	<owo:headerMenu image="${headermenu_0_106 }"
		imageAlt="${headermenu_0_016 }" list="${headermenucats }"
		menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />
	<owo:title1row title="${legaltitle }" />
	<p><b>Primera.- Compañía Organizadora</b></p>
<p>La sociedad Onwhyon SL con nombre comercial Onwhyon (en adelante Onwhyon) tiene previsto realizar una promoción dentro del ámbito territorial de España, para promocionar contenidos Premium comercializados por Onwhyon en el portal wap “Emoción” de Telefonica España S.A.U (en adelante Telefónica)  y que desarrollará de acuerdo con las siguientes bases. Telefonica está exonerada de la responsabilidad de la promoción descrita a continuación al ser una promoción organizada por Onwhyon.</p>
<p><b> Segunda.- Fechas de celebración</b></p>
<p>La promoción se desarrollará entre el día 4 de Diciembre del 2.014  a las 00:00:00 y el 7 de Enero del 2.015 23:59:59, estando ambas fechas incluidas. </p>
 <p><b>Tercera.- Legitimación para participar</b></p>
<p>Participarán en la presente promoción de ámbito nacional, y de acuerdo con lo previsto en las presentes bases, todas aquellas personas físicas que:</p>
<p>Hayan realizado una nueva alta en el servicio de suscripción de 0,99 €/semana dentro del portal Vida Saludable, con conexión 3G/HSDPA/EDGE/GPRS; el cual se encuentra ubicado en el portal emoción de Telefonica. La mera alta en la suscripción, aun no siendo consciente de la presente promoción, supone la participación sin reservas en ella.</p>
<p>Todo ello entre el día entre el día 4 de Diciembre del 2.014  a las 00:00:00 y el 7 de Enero del 2.015 23:59:59, estando ambas fechas incluidas.</p>
<p>La participación en esta acción promocional es gratuita, no implica para el consumidor incremento alguno en el precio de los productos adquiridos, sólo deberá pagar el precio habitual de la suscripción a 0,99 €/semana (IVA Incluido). </p>
<p>Quedan excluidos de esta promoción todo el personal laboral de Onwhyon y de Telefonica  España, S.A, así como las personas que mantengan una relación mercantil de agencia con Telefonica España, S.A. o dependan de los mismos, las empresas contratadas por Telefonica España para la realización de la presente promoción y los empleados de éstas.</p>
<p>En el caso de que alguna de estas personas participara en la promoción y resultara ganadora, en ningún caso se le otorgará el premio correspondiente y éste pasará al  suplente que corresponda. </p>
<p>Es requisito para participar en la promoción la aceptación, en su totalidad, de las presentes bases.</p>
<p><b> Cuarta.- Mecánica de la promoción/Ganadores</b></p>
<p>Entre todos los participantes que entre 04 de Diciembre del 2.014 00:00:00 y el 7 de Enero  del 2.015 23:59:59 hayan realizado un alta de 0,99 €/semana dentro del portal wap Vida Saludable en emoción, se elaborará una lista. En dicha lista, los participantes que hayan realizado un alta de 0,99 €/semana constarán ordenados ascendentemente según la fecha de realización del alta. A cada participante se le asignará un número ordinal, comenzando la numeración por el número uno, y siguiendo en orden ascendente, hasta el número total de participantes que hayan cumplido con lo establecido en estas bases durante la vigencia de la presente promoción.</p>
<p>Del listado se extraerá 1 número  de teléfono del ganador que ocupe la posición 500 </p>
<p>Cuando el número de participantes sea inferior a 500, se considerará ganadora la posición 250 y si tampoco hubiera ese número de participantes entonces lo será la posición 125, y así sucesivamente y en función del número total de participantes.</p>
<p>En el caso de no poder contactar con el ganador el premio pasará automáticamente a un suplente. Los suplentes serán las personas que ocupen las posiciones 501,502,503,504,505 del listado nombrado en el cuarto punto de estas bases. En caso de no contactar con el primer suplente, Onwhyon contactará  a un segundo manteniendo el criterio anterior (502). En caso el número de participantes sea inferior a 500, se consideraran suplentes las posición 251 y así sucesivamente. Se designaran hasta un total de 5 suplentes. Si no se contactara con ninguno, el premio quedará desierto.</p>
<p>El número total de ganadores será de 1 y le será entregado 1 Tarjeta Regalo de Decathlon por valor de 100€.</p>
<p><b>Quinta: Condiciones</b></p>
<p>Los ganadores, o suplentes en su caso, para poder acceder al premio y tener derecho a él, deberán residir dentro del territorio nacional, ser cliente de Telefonica y haber realizado un alta de 0,99 €/semana dentro del portal wap Vida Saludable en el periodo de la promoción. </p>

<p>Los ganadores deberán estar al corriente del pago de cuantas cantidades se hayan generado tanto en virtud de cuotas como por consumo que pudieran tener contratado. Si no fuera así perderá el derecho al premio.</p>
<p>En caso de ser cliente prepago no se admitirá un ganador que realice un uso fraudulento de la tarjeta, y si fuera así, perderá el derecho al premio.</p>
<p><b>Sexta: Gestión de la entrega del premio </b></p>
<p>Una vez conocido el ganador, Onwhyon contactará con el ganador vía telefónica, dentro de los 30 días hábiles siguientes al fin de la promoción, y acordará la forma de entrega del premio directamente con el ganador/a. El usuario deberá aceptar o renunciar al citado premio de modo expreso. </p>
<p><b> Séptima: Premio</b></p>
<p>El premio otorgado consiste en una Tarjeta Regalo de Decathlon por valor de 100€.</p>
<p>Los ganadores de la presente promoción no podrán canjear el premio ganado por otro distinto ni por la cantidad equivalente al mismo en metálico. </p>
<p><b> Octava: Derechos de imagen</b></p>
<p>Los ganadores autorizan a las empresas organizadoras a reproducir y utilizar su nombre y apellidos y otros datos, así como de su imagen, en cualquier actividad publi-promocional relacionada con la promoción en que ha resultado ganador sin que dicha utilización le confiera derecho de remuneración o beneficio alguno con excepción hecha de la entrega del premio ganado.</p>
<p><b>Novena: Protección de datos personales</b></p>
<p>De conformidad con lo establecido en la Ley Orgánica 15/1999 de Protección de Datos de Carácter Personal, Onwhyon informa a los participantes en la promoción de que los datos que le sean facilitados de forma voluntaria serán conservados a los únicos efectos del desarrollo de la promoción, celebración del promoción, entrega del premio y cumplimiento de lo dispuesto en la presentes Bases. </p>
<p><b>Décima: Varios</b></p>
<p>1 Se establece un periodo máximo de recepción de reclamaciones que se remitirán a la dirección de correo electrónico (hola@onwhyon.com), por lo que transcurrido 1 mes desde la fecha fin de la promoción no se atenderá ninguna reclamación relacionada con esta promoción.</p>
<p>2. Para toda cuestión litigiosa que pudiera dimanar de la presente promoción, tanto Telefonica España como los ganadores y participantes en la misma, hacen expresa renuncia a cualquier fuero que pudiera corresponderles, y expresamente se someten a los Juzgados y Tribunales de Madrid.</p>
<p>3.Onwhyon se reserva la facultad de cancelar, suspender o modificar las presentes bases, así como la organización, y/o gestión de la presente promoción así como de ofrecer un premio de valor equivalente.</p>
<p>4.Telefonica se exime de toda responsabilidad relativa a esta promoción.</p>
	<owo:footerLegalMovistar />

	<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE %>" />
</body>
</html>
