﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/touch" prefix="owo" %>
<%@ include file="../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<%@ include file="../inc/headTouch.jsp" %>

	<body>
		
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }" list="${headermenucats }"
			menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />
			<owo:title1row title="${title1row_0_002 }" />
			<div class="container"><p>${searcherrtxt }</p></div>
			<div class="moduloA">
				<owo:article1rowBcnts list="${article2colos_1 }" pag="<%=PRM + CNT %>" />
			</div>
			<div class="container"><p>Realizar otra búsqueda</p></div>
			<owo:search placeholder="Buscar" submitlabel="Buscar" />
			<owo:catList1col list="${article2col_0 }" pag="<%=CAT %>"/>
		</div>
	</body>
</html>